---
layout: markdown_page
title: "Product Direction - Provision: SM Provisioning"
description: "Category direction page for SM Provisioning within group Provision."
canonical_path: "/direction/fulfillment/provision/sm-provisioning"
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## Mission
Provide customers with a seamless experience activating features for GitLab Self-Managed subscriptions while enabling data-driven renewals and additional purchases.


**Additional details coming soon!**